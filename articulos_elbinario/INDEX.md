Lista de artículos:

* [Privacidad digital, software libre y otras utopías](https://gitlab.com/terceranexus6/libreta/-/blob/master/articulos_elbinario/15012021.md) o enlace a [El Binario](https://elbinario.net/2021/01/15/privacidad-digitalsoftware-libre-y-otras-utopias/)
* [Eventos, Call for Papers y mujeres en tecnología](https://gitlab.com/terceranexus6/libreta/-/blob/master/articulos_elbinario/11022021.md) o enlace a [El Binario](https://elbinario.net/2021/02/11/mujeresyeventos/)
* [Por qué seguridad y privacidad no son opuestos](https://gitlab.com/terceranexus6/libreta/-/blob/master/articulos_elbinario/11032021.md) o enlace a [El Binario](https://elbinario.net/2021/03/11/por-que-seguridad-y-privacidad-no-son-opuestos/)
* [Tecnología libre en tiempos difíciles](https://gitlab.com/terceranexus6/libreta/-/blob/master/articulos_elbinario/10042021.md) o enlace a [El Binario](https://elbinario.net/2021/04/10/tecnologia_en_tiempos_dificiles/)
* [We are the LAB](https://gitlab.com/terceranexus6/libreta/-/blob/master/articulos_elbinario/26042021.md) o enlace a [El Binario](https://elbinario.net/2021/04/26/we-are-the-lab/)
* [Sobre las redes digitales](https://gitlab.com/terceranexus6/libreta/-/blob/master/articulos_elbinario/27052021.md) o enlace a [El Binario](https://elbinario.net/2021/05/27/sobre-las-redes-digitales/)
* [Espacios comunes digitales y tangibles](https://gitlab.com/terceranexus6/libreta/-/blob/master/articulos_elbinario/07072021.md) o enlace a [El Binario](https://elbinario.net/2021/07/07/espacios-comunes-digitales-y-tangibles/)
* [Fuera Google de las aulas](https://gitlab.com/terceranexus6/libreta/-/blob/master/articulos_elbinario/12072021.md) o enlace a [El Binario](https://elbinario.net/2021/07/12/fuera-google-de-las-aulas/)
* [El mito de los cuidados digitales](https://gitlab.com/terceranexus6/libreta/-/blob/master/articulos_elbinario/07092021.md) o enlace a [El Binario](https://elbinario.net/2021/09/07/el-mito-de-los-cuidados-digitales/)
* [Lo que fue y siempre ha sido](https://gitlab.com/terceranexus6/libreta/-/blob/master/articulos_elbinario/18102021.md) o enlace a [El Binario](https://elbinario.net/2021/10/18/lo-que-fue-y-siempre-ha-sido/)
