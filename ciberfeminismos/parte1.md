# Introducción

Cuando se habla de la tecnología, libre o privativa, el concepto abarca mucho más que una mera cuestión técnica. Incluye un acercamiento histórico, unos prejuicios culturales, y la presunción de muchos conceptos que se aceptan casi como mágicos o religiosos. 

Los problemas derivados de los servicios privativos que gestionan una ingente cantidad de datos personales tiene dos caras. La primera es el uso de esos datos por parte de dichos servicios, problemas que pueden entenderse más claramente en “Don’t be evil” de Rana Foroohar o en “Armas de destrucción matemática” por Cathy O’Neil. Ambas autoras ponen de manifiesto el impacto del tratamiento indiscriminado de datos y contesta a aquella pregunta tan recurrente de “¿por qué preocuparme por mi privacidad si no tengo nada que esconder?”. Algo de lo que no se habla frecuentemente es que nuestros datos personales no son sólo nuestros. Y que por datos privados se puede entender mucho más que nombre, apellido o DNI. 

Por poner un ejemplo, en nuestras redes sociales (Facebook, Twitter, Instagram, TikTok y otros) las imágenes que se suben en una situación casual albergan mucha más información de la que intuitivamente cabe imaginarse. Para empezar cada archivo multimedia subido contiene una serie de metadatos (más o menos modificables dependiendo de la situación) la cual puede incluir localización, fecha, hora (no solo de subida si no de cuándo se tomó esa imagen), dispositivo desde el cuál se ha tomado, entre otros. Sólo esta información puede comprometer la integridad y la ciber-seguridad de una persona pues, por ejemplo, pueden mandarse ataques mucho más específicos, como explicaré más adelante. Otro detalle es lo que aparece en esa imagen: una imagen aparentemente inocua puede mostrar caras o situaciones de otras personas (conocidas o no) comprometiendo sin su permiso su propia privacidad. No sólo caras: tarjetas de crédito (por ejemplo en una cafetería), conversaciones de chat y similar (por ejemplo, en una foto de un metro o un bus) o de unas llaves (existe software que analiza la forma de las llaves en fotos y se pasan a un archivo que puede imprimirse en 3D). Teniendo en cuenta el impacto del contenido multimedia en las redes, ¿por qué no existe esa concienciación generalizada? 

Es evidente que las redes tienen un impacto grandísimo, es por eso que la publicidad siempre encuentra un hueco donde colarse entre tu TL (tweet line) de Twitter o tus laterales de Facebook o las publicaciones y stories de Instagram. Estos servicios y el equipo técnico de detrás ganan mucho dinero, y sin embargo hacerse una cuenta es gratuito. Los datos se utilizan para lanzar los anuncios determinados, y esos datos cedidos (tanto los de tu perfil como los que generas con esas imágenes o participando en esa red o incluso más intrusivos) pasan de mano en mano en principio para generar anuncios personalizados, pero una de esas manos puede ser spam o ciberatacantes. ¿Alguna vez te has preguntado como consiguen esas estafas de teléfono tu información? ¿O cómo te llega una cadena con un virus por Whatsapp? Whatsapp y Facebook pertenecen a la misma compañía y tienen datos cruzados por defecto. 

## Biblio te este trocito:

- “Don’t be evil” de Rana Foroohar
- “Armas de destrucción matemática” por Cathy O’Neil.

## Otras lecturas recomendadas:

## Manifiesto sobre Xenofeminismo de Laboria cuboniks https://laboriacuboniks.net/manifesto/xenofeminism-a-politics-for-alienation/ 

Contexto:
Un manifiesto creado por un grupo de acción y teorías feministas (con sus correspondientes interseccionalidades – de ahí el “Xeno”), que quiere poner de manifiesto la influencia de la tecnología en la sociedad desde esa perspectiva feminista. Incluyendo, por ejemplo, la soberanía de la información.

Cómo leerlo:
Ni como un manual, ni como un relato. Es más bien una colección de pensamientos y una reivindicación. Una lee este manifiesto como quien lee panfletos en una manifiestación. Sin embargo algunas lecturas relacionadas con el grupo son más especificas, puedes encontrar todas aquí https://laboriacuboniks.net/books/.

## Ciberfeminismos (https://www.todostuslibros.com/libros/ciberfeminismo_978-84-948782-6-8 )

Contexto
También habla de laboria Cuboniks, pero en este caso habla también de otros grupos similares o de arte digital/electrónico y su relación con el feminismo. 

Cómo leerlo
A mi parecer es una enciclopedia y una colección de referencias a las que mirar y estudiar cuando queremos construir una imagen mental de la tecnología en el feminismo. Pese a la portada, es inclusivo en contenido y no habla desde una perspectiva TERF o discriminatoria, me gusta puntualizarlo porque al principio la portada me dio esa sensación.

## Interseccionalidad (https://www.todostuslibros.com/libros/interseccionalidad_978-84-18684-16-6)

Contexto
Aunque no directamente relacionado con la tecnología en si, es una lectura interesante para comprender cómo afectan las diversas situaciones en las que nos encontramos al desarrollo del feminismo, concepto que quiero introducir al hablar de tecnologías diversas vs tecnologías de globalización. La mayoría de redes sociales privativas (es decir, aquellas cuyo código, recursos y gestión no están expuestas ni dependen de la colaboración de la comunidad) apuestan por la alineación de contenido y una cultura globalizada, que generalmente implica una cultura occidentalizada y con una serie de parámetros específicos. Este diseño de la tecnología agrava los problemas de acceso y creación de contenidos de minorías o personas en contextos social y culturalmente diversos. El libro Interseccionalidad ayuda a comprender la necesidad de una tecnología cuya soberanía sea más abierta, a mi parecer.

Cómo leerlo
Como un ensayo. Ayuda a trabajar sobre una misma y su entorno, en nuestro caso con el seminario, el tecnológico.

## Después de Internet (https://www.todostuslibros.com/libros/despues-de-internet_978-84-7774-900-4 )

Contexto
En la linea de lo mencionado anteriormente, para aprender más sobre soberanía digital y el impacto del software libre en comunidades minoritarias, esta lectura es genial. Además el prólogo es de Ursula K Le Guin. 

Cómo leerlo
Como un ensayo para comprender mejor el software libre fuera de un contexto puramente técnico, además de que habla también del impacto de Internet en la sociedad de un modo no liberal.  

## Subversión (https://www.todostuslibros.com/libros/subversion_978-84-16946-19-8#synopsis)

Contexto
Pone de manifiesto el peligro de la vigilancia masiva y el tratamiento abusivo de datos en Internet. Una explicación en profundidad del daño de Google, Amazon y otras empresas similares.

Cómo leerlo
Es corto, y ayuda a hacerse una idea sobre el peligro del uso indiscriminado de datos del que tanto se escucha hablar en según qué círculos, de un modo no-paranóico. Esto es impotante porque a veces se pierde credibilidad al tratar de hablar desde el miedo, mientras que este ensayo pretende hablar desde la reivindicación.

## La era del capitalismo de vigilancia (https://www.todostuslibros.com/libros/la-era-del-capitalismo-de-la-vigilancia_978-84-493-3693-5#synopsis )

Contexto
La influencia del capitalismo en la tecnología y el modelo de empresa que utiliza datos.



## Anexo de conceptos:

Software libre: es un tipo de desarrollo de tecnología que apuesta por crear herramientas y contenido digital en comunidad, compartiendo y de forma abierta (a otras comunidades, a otras áreas, etc) y con mucha documentación y manuales para que la gente pueda aprender. Existe desde hace muchos años como una comunidad establecida, sin jerarquía y con el objetivo de crear y compartir en bucle.

Licencia libre: son licencias que se utilizan para preservar la tecnología que se crea bajo los términos del software libre, así como otros contenidos audiovisuales. Algunos ejemplos famosos son las licencias CC (creative commons) para obras de muchos tipos. Hay otras muchas licencias, cada una con detalles diferentes.

Código o código fuente: se trata del esqueleto de cualquier programa informático, web o servicio. Está escrito en un lenguaje que entienden humanos y ordenadores, de modo que alguien escribe una orden en ese lenguaje y el ordenador/servidor lo interpreta para crear ese programa que estás utilizando. A esos lenguajes se les llama lenguajes de programación, algunos conocidos son: Python, C, C++, ruby, java… Hace muchos años se usaban BASIC, ensamblador (este aún, en realidad), y otros similares.

Fediverso: una colección de redes sociales que se han hecho con software libre, y por lo tanto llevan varias comunidades, en lugar de una empresa. Se encuentran todas aquí: https://fediverse.party 

Vigilancia masiva: la técnica mediante la cual se manipulan datos que se utilizan en internet para perfilar nuestros gustos, vida, tiempos, etc con la idea de generar campañas de marketing agresivas o manipulación de información mostrada.

Metadatos: datos intrínsecos dentro de elementos digitales como fotos, audio o video, que incluyen información extra sobre ese elemento y que se utilizan para varias cosas, por ejemplo: fechas, autores, dispositivos que los han generado…

Sistema operativo: se refiere al sistema que hace funcionar tu ordenador. Está en tu ordenador al encenderlo, y gestiona los programas que funcionan sobre él, los USB que metes, los periféricos (ratón, teclado, etc) y, básicamente, hace de intermediario entre la electrónica del ordenador y tú. Muchos vienen por defecto instalados no por que sean mejores, si no porque tienen un contrato con esa marca de ordenadores (como Windows) o porque los crean los mismos fabricantes (MacOS).

(GNU) Linux: Es un sistema operativo hecho con software libre. Hay muchos tipos, dependiendo de la comunidad que haya detrás o las necesidades de cada persona. Algunos de los más famosos son Debian, Ubuntu, Lubuntu, Mint, Manajaro, Elementary, Arch… Se pueden instalar sin tener conocimientos técnicos, respondiendo a preguntas en varias pantallas, como haces para configurar Windows cuando viene por defecto. Algunos de los favoritos para principiantes son Ubuntu (https://ubuntu.com/download/desktop ) o Mint (https://linuxmint.com/download.php), que a su vez vienen varias versiones depende de los gráficos que quieras o los recursos que tenga tu ordenador.  

Ciberseguridad: todo lo que implique seguridad en el entorno digital, por ejemplo la protección contra ataques informáticos.

Ciberatacantes: personas que utilizan la tecnología para atacar, ya sea a través de engaños o aprovechando vulnerabilidades en programas y servicios. 

SPAM: contenido no necesariamente malicioso pero si indeseado que se nos manda sin nuestro permiso. Puede acabar en un ataque informático o una estafa pero no tiene por qué, a veces solo es marketing agresivo.

Virus: un tipo de programa que está diseñado para atacar dispositivos informáticos ya sea ordenador, un móvil u otro. No necesariamente nos tenemos que dar cuenta de que está ahí, ni tiene por qué necesariamente afectar al funcionamiento normal del teléfono o el ordenador, pues algunos son de espio
